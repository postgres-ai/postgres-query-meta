FROM golang:1.21 as build
ARG cmd=server
COPY . /go/app
WORKDIR /go/app
RUN go mod download
RUN GO111MODULE=on go build -o ./bin/postgres-query-meta ./cmd/${cmd}/...

FROM ubuntu:18.04 as production
COPY --from=build /go/app/bin/postgres-query-meta ./postgres-query-meta
ENTRYPOINT ["./postgres-query-meta"]
