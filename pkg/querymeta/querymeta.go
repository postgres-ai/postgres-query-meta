// Package querymeta provides meta information for queries.
package querymeta

import (
	"github.com/lfittl/pg_query_go"
	"github.com/pkg/errors"
)

// QueryMeta defines meta information about query.
type QueryMeta struct {
	Query           string                 `json:"query"`
	NormalizedQuery string                 `json:"normalizedQuery"`
	Fingerprint     string                 `json:"fingerprint"`
	Tree            pg_query.ParsetreeList `json:"tree"`
}

// GatherMeta processes query and provides meta information about it.
func GatherMeta(query string) (*QueryMeta, error) {
	normalized, err := pg_query.Normalize(query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to normalize query")
	}

	tree, err := pg_query.Parse(query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query tree")
	}

	fingerprint := tree.Fingerprint()

	meta := &QueryMeta{
		Query:           query,
		NormalizedQuery: normalized,
		Fingerprint:     fingerprint,
		Tree:            tree,
	}

	return meta, nil
}
