// Package httpserver provides HTTP server for query meta gathering.
package httpserver

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"time"

	"github.com/pkg/errors"
	"github.com/segmentio/ksuid"
)

type key int

const (
	requestIDKey key = 0

	serverOff int32 = 0
	serverOn  int32 = 1

	readTimeout     = 5 * time.Second
	writeTimeout    = 10 * time.Second
	idleTimeout     = 15 * time.Second
	shutdownTimeout = 30 * time.Second
)

// Server defines server configuration.
type Server struct {
	ReadTimeout     time.Duration
	WriteTimeout    time.Duration
	IdleTimeout     time.Duration
	ShutdownTimeout time.Duration
	ListenAddr      string
	Logger          *log.Logger
	Router          *http.ServeMux
	Healthy         int32
}

// New defines a new instance of server.
func New() *Server {
	server := &Server{
		ReadTimeout:     readTimeout,
		WriteTimeout:    writeTimeout,
		IdleTimeout:     idleTimeout,
		ShutdownTimeout: shutdownTimeout,
	}

	return server
}

// Based on:
// https://gist.github.com/enricofoltran/10b4a980cd07cb02836f70a4ab3e72d7 by enricofoltran.

// Run runs a HTTP server.
func (s *Server) Run() error {
	s.Logger.Println("Server is starting...")

	router := http.NewServeMux()
	router.HandleFunc("/", s.index)
	router.HandleFunc("/healthz", s.healthz)
	s.Router = router

	httpServer := &http.Server{
		Addr:         s.ListenAddr,
		Handler:      s.tracing(s.logging(s.Router)),
		ErrorLog:     s.Logger,
		ReadTimeout:  s.ReadTimeout,
		WriteTimeout: s.WriteTimeout,
		IdleTimeout:  s.IdleTimeout,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		s.Logger.Println("Server is shutting down...")
		atomic.StoreInt32(&s.Healthy, serverOff)

		ctx, cancel := context.WithTimeout(context.Background(), s.ShutdownTimeout)
		defer cancel()

		httpServer.SetKeepAlivesEnabled(false)

		if err := httpServer.Shutdown(ctx); err != nil {
			s.Logger.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}

		close(done)
	}()

	s.Logger.Println("Starting server to handle requests at", s.ListenAddr)
	atomic.StoreInt32(&s.Healthy, serverOn)

	if err := httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		return errors.Wrapf(err, "could not listen on %s", s.ListenAddr)
	}

	<-done
	s.Logger.Println("Server stopped")

	return nil
}

func (s *Server) logRequest(r *http.Request, v ...interface{}) {
	requestID, ok := r.Context().Value(requestIDKey).(string)
	if !ok {
		requestID = "unknown"
	}

	s.Logger.Println(requestID, v)
}

func (s *Server) nextRequestID() string {
	return ksuid.New().String()
}
