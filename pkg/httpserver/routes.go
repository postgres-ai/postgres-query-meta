package httpserver

import (
	"net/http"
	"sync/atomic"

	"gitlab.com/postgres-ai/postgres-query-meta/pkg/querymeta"
)

func (s *Server) index(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		failNotFound(w)
		return
	}

	var queryMetaRequest QueryMetaRequest

	err := readJSON(r, &queryMetaRequest)
	if err != nil {
		failBadRequest(w, err.Error())
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")

	meta, err := querymeta.GatherMeta(queryMetaRequest.Query)
	if err != nil {
		failBadRequest(w, err.Error())
		return
	}

	if err = writeJSON(w, http.StatusOK, meta); err != nil {
		failInternalError(w)
		return
	}
}

func (s *Server) healthz(w http.ResponseWriter, r *http.Request) {
	if atomic.LoadInt32(&s.Healthy) == serverOn {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.WriteHeader(http.StatusServiceUnavailable)
}
