package httpserver

// QueryMetaRequest defines request for meta information about query.
type QueryMetaRequest struct {
	Query string `json:"query"`
}
