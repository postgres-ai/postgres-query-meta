module gitlab.com/postgres-ai/postgres-query-meta

go 1.21

require (
	github.com/lfittl/pg_query_go v1.0.2
	github.com/pkg/errors v0.9.1
	github.com/segmentio/ksuid v1.0.2
)
