package main

import (
	"log"
	"os"

	"gitlab.com/postgres-ai/postgres-query-meta/pkg/httpserver"
)

func main() {
	server := httpserver.New()

	listenAddr := os.Getenv("LISTEN_ADDR")
	if listenAddr == "" {
		listenAddr = ":3000"
	}

	server.ListenAddr = listenAddr

	server.Logger = log.New(os.Stdout, "http: ", log.LstdFlags)

	if err := server.Run(); err != nil {
		server.Logger.Fatalf(err.Error())
	}
}
