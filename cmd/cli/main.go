package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/postgres-ai/postgres-query-meta/pkg/querymeta"
)

const (
	numArgs = 2
)

func main() {
	if len(os.Args) != numArgs {
		log.Fatal("USAGE: pg_query_normalize 'QUERY'")
	}

	query := os.Args[1]

	meta, err := querymeta.GatherMeta(query)
	if err != nil {
		log.Fatal(err)
	}

	jsonBytes, err := json.MarshalIndent(meta, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(jsonBytes))
}
