# postgres-query-meta

[pg_query.go](https://github.com/lfittl/pg_query_go), packaged in a Docker container.

## Usage examples

```bash
$ docker run --rm postgresai/pg-query-normalize 'select 1 from xxx'
{
  "query": "select 1 from xxx",
  "normalizedQuery": "select $1 from xxx",
  "fingerprint": "021572992ffd8cf8db4cd699e1be95efb2f7870d81",
  "tree": [
    {
      "RawStmt": {
        "stmt": {
          "SelectStmt": {
            "distinctClause": {
              "List": {
                "items": null
              }
            },
            "intoClause": null,
            "targetList": {
              "List": {
                "items": [
                  {
                    "ResTarget": {
                      "name": null,
                      "indirection": {
                        "List": {
                          "items": null
                        }
                      },
                      "val": {
                        "A_Const": {
                          "val": {
                            "Integer": {
                              "ival": 1
                            }
                          },
                          "location": 7
                        }
                      },
                      "location": 7
                    }
                  }
                ]
              }
            },
            "fromClause": {
              "List": {
                "items": [
                  {
                    "RangeVar": {
                      "catalogname": null,
                      "schemaname": null,
                      "relname": "xxx",
                      "inh": true,
                      "relpersistence": 112,
                      "alias": null,
                      "location": 14
                    }
                  }
                ]
              }
            },
        ...
        "stmt_location": 0,
        "stmt_len": 0
      }
    }
  ]
}

$ docker run --rm postgresai/pg-query-normalize 'select ?'
{
  "query": "select ?",
  "normalizedQuery": "select ?",
  "fingerprint": "02a281c251c3a43d2fe7457dff01f76c5cc523f8c8",
  "tree": [...]
  ]
}

$ docker run --rm postgresai/pg-query-normalize 'select $11'
{
  "query": "select $11",
  "normalizedQuery": "select $11",
  "fingerprint": "02a281c251c3a43d2fe7457dff01f76c5cc523f8c8",
  "tree": [...]
}

$ docker run --rm postgresai/pg-query-normalize "select \$11, ?, 213, 'bla';"
{
  "query": "select $11, ?, 213, 'bla';",
  "normalizedQuery": "select $11, ?, $12, $13;",
  "fingerprint": "02a281c251c3a43d2fe7457dff01f76c5cc523f8c8",
  "tree": [...]
}
```

## Build

```bash
docker build --tag registry.gitlab.com/postgres-ai/postgres-query-meta:server -f Dockerfile --build-arg cmd=server .
docker build --tag registry.gitlab.com/postgres-ai/postgres-query-meta:cli -f Dockerfile --build-arg cmd=cli .
```
